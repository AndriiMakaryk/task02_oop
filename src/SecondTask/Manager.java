package SecondTask;

import java.util.List;

import java.util.stream.Collectors;

public class Manager {
	private JewerlyStore store;

	public Manager(JewerlyStore store) {
		this.store = store;
	}

	public JewerlyStore getStore() {
		return store;
	}

	public void setStore(JewerlyStore store) {
		this.store = store;
	}

	public List<SemipreciousStone> takeStonesForBeadsByWeight(int weight) {
		List<SemipreciousStone> heavyBeads = store.getStore().stream()
				                                             .filter(p -> p.getWeight() > weight)
				                                             .collect(Collectors.toList());
		return heavyBeads;
	}

	public void countOfTotalWeightAndCostBeads(List<SemipreciousStone> list) {
		int totalWeight = list.stream().mapToInt(p -> p.getWeight()).sum();
		int totalCost = list.stream().mapToInt(p -> p.getPrice()).sum();
		System.out.println("Total weight of beads: " + totalWeight + " and total cost " + totalCost);
	}

	public List<SemipreciousStone> sortStonesOfBeadsByGem(int price) {
		List<SemipreciousStone> expensiveStone = store.getStore().stream()
	                                                             .filter(p -> p.getPrice() > price)
				                                                 .collect(Collectors.toList());
		return expensiveStone;
	}

	public List<SemipreciousStone> sortBetweenTransperency(int from, int to) {
		List<SemipreciousStone> sortedStonByTransperency = store.getStore().stream()
				                                                           .filter(p -> p.getTransperency() > from && p.getTransperency() < to)
				                                                           .sorted((p1, p2) -> p1.getTransperency() - p2.getTransperency())
				                                                           .collect(Collectors.toList());

		return sortedStonByTransperency;
	}
}
