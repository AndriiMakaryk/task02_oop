package SecondTask;

import java.util.ArrayList;
import java.util.List;

public class TestProgramm {

	public static void main(String[] args) {
		/*
		 * Initialisation store of stones
		 */

		List<SemipreciousStone> store = new ArrayList<>();
		store.add(new SemipreciousStone("Diamond", 12, 16, 34));
		store.add(new SemipreciousStone("Ruby", 11, 4, 33));
		store.add(new SemipreciousStone("LapisLazuly", 7, 3, 22));
		store.add(new SemipreciousStone("Emerald", 12, 6, 17));
		store.add(new SemipreciousStone("Churghon", 7, 3, 45));
		store.add(new SemipreciousStone("Turquoise", 12, 6, 20));
		store.add(new SemipreciousStone("Hematite", 11, 3, 17));
		store.add(new SemipreciousStone("Emerald", 18, 16, 19));//
		store.add(new SemipreciousStone("Crysocolla", 28, 11, 21));
		store.add(new SemipreciousStone("TigerEye", 15, 7, 24));
		store.add(new SemipreciousStone("Surgilite", 17, 8, 27));
		store.add(new SemipreciousStone("Pyrite", 21, 4, 25));
		store.add(new SemipreciousStone("Tourmaline", 37, 5, 33));
		store.add(new SemipreciousStone("Quartz", 12, 5, 41));
		store.add(new SemipreciousStone("Malachite", 12, 6, 42));
		store.add(new SemipreciousStone("RoseQuartz", 71, 13, 19));
		store.add(new SemipreciousStone("MoseAgate", 12, 26, 38));
		store.add(new SemipreciousStone("Obsidian", 7, 3, 39));
		store.add(new SemipreciousStone("Jasper", 12, 16, 41));
		store.add(new SemipreciousStone("Amethyst", 12, 16, 12));
		store.add(new SemipreciousStone("LaceAgate", 27, 31, 6));
		store.add(new SemipreciousStone("LapisLazoli", 12, 6, 14));
		store.add(new SemipreciousStone("Sapphire", 37, 23, 23));
		store.add(new SemipreciousStone("BlackQuartz", 10, 16, 17));
		/*
		 * Set all values to JewerlyStore
		 */
		JewerlyStore stoneStore = new JewerlyStore();
		stoneStore.setStore(store);
		/*
		 * Instance of Manager and invoke the method takeStonesForBeadsByWeight()
		 */

		Manager engryManager = new Manager(stoneStore);
		List<SemipreciousStone> takeAwayForBeads = engryManager.takeStonesForBeadsByWeight(34);
		System.out.println("==============Heavy beads==================");
		takeAwayForBeads.forEach(p -> {
			System.out.println(p.toString());
		});
		/*
		 * invoke the method countOfTotalWeightAndCostBeads(takeAwayForBeads) with the parameters
		 */
		System.out.println();
		System.out.println("===Total weight and cost of chosen beads===");
		engryManager.countOfTotalWeightAndCostBeads(takeAwayForBeads);
		/*
		 * invoke the method sortStonesOfBeadsByGem() with the parameter
		 */
		List<SemipreciousStone> breads = engryManager.sortStonesOfBeadsByGem(20);
		System.out.println();
		System.out.println("==============Rich beads==================");
		breads.forEach(p -> {
			System.out.println(p.toString());
		});
		/*
		 * invoke the method sortBetweenTransperency() with the parameters
		 */
		
		System.out.println();
		System.out.println("===========Sorted by transperency============");
		List<SemipreciousStone> transperencySorted = engryManager.sortBetweenTransperency(24, 30);
		transperencySorted.forEach(p -> {
			System.out.println(p.toString());
		});
	}
}
