package SecondTask;

public class SemipreciousStone {
	private String name;
	private int weight;
	private int price;
	private int transperency;
	
	public SemipreciousStone() {

	}
	public SemipreciousStone(String name, int weight, int price, int transperency) {
		this.name = name;
		this.weight = weight;
		this.price = price;
		this.transperency = transperency;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getTransperency() {
		return transperency;
	}

	public void setTransperency(int transperency) {
		this.transperency = transperency;
	}

	@Override
	public String toString() {
		return "SemipreciousStone [name=" + name + ", weight=" + weight + ", price=" + price + ", transperency="
				+ transperency + "]";
	}
}
